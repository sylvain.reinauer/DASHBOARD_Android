package com.greengt.sylvain.ggt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.TreeMap;

import static com.greengt.sylvain.ggt.R.color.colorBlueAccent;
import static com.greengt.sylvain.ggt.R.color.colorOrange;
import static com.greengt.sylvain.ggt.R.color.colorText;

public class CAN_Frame_Adapter extends ArrayAdapter<String> {

    private Context context;
    private TreeMap<Integer, CAN_Frame> treeMap;
    private Integer[] mapKeys;
    int selectedPosition = 0;
    TextView format;
    TextView id;
    TextView data;
    TextView timeOut;
    TextView dlc;

    public CAN_Frame_Adapter(Context context, TreeMap<Integer, CAN_Frame> treeMap) {
        super(context, R.layout.can_frame_lv_row);

        this.context = context;
        this.treeMap = treeMap;
        mapKeys = this.treeMap.keySet().toArray(new Integer[getCount()]);
    }

    public int getCount() {
        return treeMap.size();
    }

    public String getItem(int position) {
        return String.valueOf(treeMap.get(mapKeys[position]));
    }

    //public String getItemId(int position) {return mapKeys[position];}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater deviceInflater = LayoutInflater.from(getContext());
        final View listViewRow = deviceInflater.inflate(R.layout.can_frame_lv_row, parent, false);

        final CAN_Frame can_frame = (CAN_Frame) treeMap.get(mapKeys[position]);

        String idString = String.format("0x%X",  can_frame.getId());
        String timeOutString = String.format("time: %2.1f[s]",  can_frame.getTimeOut());
        String dataString = can_frame.getStringDataHex();
        String formatString = "" + can_frame.getFORMAT();
        String dlcString = String.format("dlc: %1d",  can_frame.getDLC());

        timeOut = (TextView) listViewRow.findViewById(R.id.timeOut);
        data = (TextView) listViewRow.findViewById(R.id.data);
        format = (TextView) listViewRow.findViewById(R.id.format);
        id = (TextView) listViewRow.findViewById(R.id.id);
        dlc = (TextView) listViewRow.findViewById(R.id.dlc);


        timeOut.setText(timeOutString);
        data.setText(dataString);
        id.setText(idString);
        dlc.setText(dlcString);
        format.setText(formatString);
        this.notifyDataSetChanged();
        return listViewRow;
    }
}
