package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     17.08.2018
 * Fichier:  Fragment FragmentDashboard
 * Sujet:    Le fragment FragmentDashboard est le thread principale de l'UI drécrit dans le fichier fragment_dashboard.xml
 *
 */
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragmentDashboard extends Fragment {

    private static final Float GAUGE_SPEED_ANGLE_MIN = Float.valueOf(0);
    private static final Float GAUGE_SPEED_ANGLE_MAX = Float.valueOf(200);
    private static final Float GAUGE_TEMP_ANGLE_MIN = Float.valueOf(-60);
    private static final Float GAUGE_TEMP_ANGLE_MAX = Float.valueOf(60);

    private Thread threadUpdateScreen;
    private CAN_Demo threadCan_Demo;
    ProgressBar progressBarBatCapacity;
    ProgressBar progressBarBatPowerSupply;
    ProgressBar progressBarBatPowerRecharge;
    ProgressBar progressBarPwtPowerSupply;
    ProgressBar progressBarPwtPowerConsumption;
    ProgressBar progressBarH2Level;
    ProgressBar progressBarFCPowerSupply;
    ProgressBar progressBarThrottle;
    ProgressBar progressBarBrake;
    ImageView imageViewIndicatorSpeed;
    ImageView imageViewIndicatorBatTemp;
    Gauge gaugeSpeed;
    Gauge gaugeBatTemp;
    Gauge gaugePwtTemp;
    Gauge gaugeFcTemp;
    TextView textViewFcPower;
    TextView textViewH2Capacity;
    TextView textViewPwtPower;
    TextView textViewBatPower;
    TextView textViewBatCapacity;
    TextView textViewBatTemp;
    TextView textViewFcTemp;
    TextView textViewPwtTemp;
    TextView textViewSpeed;

    ImageView buttonBattery;
    ImageView buttonPowertrain;
    ImageView buttonFuelcell;
    Drawable buttonBatBackground;
    Drawable buttonPwtBackground;
    Drawable buttonFcsBackground;


    double i;
    CAN_Parameter can_parameter = CAN_Parameter.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        threadUpdateScreen = new Thread(){
            public void run(){
                while(getActivity()!=null){

                    updateScreen();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }}
            }
        };
        threadUpdateScreen.start();
        //threadCan_Demo.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        threadUpdateScreen.interrupt();
        //threadCan_Demo.interrupt();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        progressBarBatCapacity = (ProgressBar)view.findViewById(R.id.progressBarBatCapacity);
        progressBarBatPowerRecharge = (ProgressBar)view.findViewById(R.id.progressBarBatPowerRegen);
        progressBarBatPowerSupply = (ProgressBar)view.findViewById(R.id.progressBarBatPowerConso);
        progressBarPwtPowerConsumption = (ProgressBar)view.findViewById(R.id.progressBarPwtPowerConso);
        progressBarPwtPowerSupply = (ProgressBar)view.findViewById(R.id.progressBarPwtPowerRegen);
        progressBarFCPowerSupply = (ProgressBar)view.findViewById(R.id.progressBarFcPower);
        progressBarH2Level =(ProgressBar)view.findViewById(R.id.progressBarH2Tank);
        progressBarThrottle = (ProgressBar)view.findViewById(R.id.progressBarThrottle);
        progressBarBrake = (ProgressBar)view.findViewById(R.id.progressBarBrake);

        imageViewIndicatorBatTemp =(ImageView) view.findViewById(R.id.imageViewIndicatorBatTemp);
        gaugeSpeed = new Gauge(view.findViewById(R.id.imageViewGaugeSpeed),
                view.findViewById(R.id.imageViewIndicatorSpeed), GAUGE_SPEED_ANGLE_MIN, GAUGE_SPEED_ANGLE_MAX);
        gaugeBatTemp = new Gauge(view.findViewById(R.id.imageViewGaugeBatTemp),
                view.findViewById(R.id.imageViewIndicatorBatTemp), GAUGE_TEMP_ANGLE_MIN, GAUGE_TEMP_ANGLE_MAX);
        gaugePwtTemp = new Gauge(view.findViewById(R.id.imageViewGaugePwtTemp),
                view.findViewById(R.id.imageViewIndicatorPwtTemp), GAUGE_TEMP_ANGLE_MIN, GAUGE_TEMP_ANGLE_MAX);
        gaugeFcTemp = new Gauge(view.findViewById(R.id.imageViewGaugeFcTemp),
                view.findViewById(R.id.imageViewIndicatorFcTemp), GAUGE_TEMP_ANGLE_MIN, GAUGE_TEMP_ANGLE_MAX);

        textViewBatCapacity = (TextView) view.findViewById(R.id.textViewBatCapacity);
        textViewBatPower = (TextView) view.findViewById(R.id.textViewBatPwr);
        textViewBatTemp = (TextView) view.findViewById(R.id.textViewBatTemp);
        textViewFcPower = (TextView) view.findViewById(R.id.textViewFcPwr);
        textViewFcTemp = (TextView) view.findViewById(R.id.textViewFcTemp);
        textViewH2Capacity = (TextView) view.findViewById(R.id.textViewFcH2Capacity);
        textViewPwtPower = (TextView)view.findViewById(R.id.textViewPwtPower);
        textViewPwtTemp = (TextView) view.findViewById(R.id.textViewPwtTemp);
        textViewSpeed = (TextView) view.findViewById(R.id.textViewSpeed);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Oswald-Medium.ttf");
        textViewBatCapacity.setTypeface(typeface);
        textViewBatPower.setTypeface(typeface);
        textViewBatTemp.setTypeface(typeface);
        textViewFcPower.setTypeface(typeface);
        textViewFcTemp.setTypeface(typeface);
        textViewH2Capacity.setTypeface(typeface);
        textViewPwtPower.setTypeface(typeface);
        textViewPwtTemp.setTypeface(typeface);
        textViewSpeed.setTypeface(typeface);

        buttonBattery = (ImageView)view.findViewById(R.id.btnBAT);
        buttonPowertrain = (ImageView)view.findViewById(R.id.btnPWT);
        buttonFuelcell = (ImageView)view.findViewById(R.id.btnFC);
        return  view;
    }

public void updateScreen(){
        //i =((ActivityMain)getActivity()).getH2TankPr();
    getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {

            gaugeSpeed.setAngleFromPercent(can_parameter.channel_CarSpeed.getPercentValue());
            gaugeFcTemp.setAngleFromPercent(can_parameter.channel_WaterInletTemp.getPercentValue());
            gaugePwtTemp.setAngleFromPercent(can_parameter.channel_WaterOutletTemp.getPercentValue());
            gaugeBatTemp.setAngleFromPercent(can_parameter.channel_OilInletTemp.getPercentValue());

            progressBarBatCapacity.setProgress((int)can_parameter.channel_BatSOC.getPercentValue());

            if(can_parameter.channel_BatPower.getVariable() >= 0){
                progressBarBatPowerRecharge.setProgress((int)can_parameter.channel_BatPower.getPercentValue());
                progressBarBatPowerSupply.setProgress(0);
            }
            else{
                progressBarBatPowerSupply.setProgress((int)can_parameter.channel_BatPower.getPercentValue());
                progressBarBatPowerRecharge.setProgress(0);
            }

            if(can_parameter.channel_PwtPower.getVariable() >= 0){
                progressBarPwtPowerConsumption.setProgress((int)can_parameter.channel_PwtPower.getPercentValue());
                progressBarPwtPowerSupply.setProgress(0);
            }
            else {
                progressBarPwtPowerSupply.setProgress((int) can_parameter.channel_PwtPower.getPercentValue());
                progressBarPwtPowerConsumption.setProgress(0);
            }

            progressBarFCPowerSupply.setProgress((int)can_parameter.channel_FcsPower.getPercentValue());
            progressBarH2Level.setProgress((int)can_parameter.channel_H2TankPressure.getPercentValue());
            progressBarThrottle.setProgress((int)can_parameter.channel_ThrottlePosition.getPercentValue());
            progressBarBrake.setProgress((int)can_parameter.channel_FrontBrakePressure.getPercentValue());
            textViewBatCapacity.setText(""+(int)can_parameter.channel_BatSOC.getVariable());
            textViewBatPower.setText(""+(int)can_parameter.channel_BatPower.getVariable());
            textViewBatTemp.setText(""+(int)can_parameter.channel_OilInletTemp.getVariable());
            textViewFcPower.setText(""+(int)can_parameter.channel_FcsPower.getVariable());
            textViewFcTemp.setText(""+(int)can_parameter.channel_WaterInletTemp.getVariable());
            textViewH2Capacity.setText(""+(int)can_parameter.channel_H2TankPressure.getVariable());
            textViewPwtPower.setText(""+(int)can_parameter.channel_PwtPower.getVariable());
            textViewPwtTemp.setText(""+(int)can_parameter.channel_WaterOutletTemp.getVariable());
            textViewSpeed.setText(""+(int)can_parameter.channel_CarSpeed.getVariable());

            buttonBattery.setImageResource(R.drawable.ic_bat_on);
            buttonFuelcell.setImageResource(R.drawable.ic_fc_on);
            buttonPowertrain.setImageResource(R.drawable.ic_pwt_on);
        }


    });
}

    public void determineButtonBackground(){
        switch ((int)can_parameter.channel_BmsState.getVariable()){
            case 0x110:
                buttonBatBackground = getActivity().getDrawable(R.drawable.ic_bat_on);
                break;
            case 0x10:
                buttonBatBackground = getActivity().getDrawable(R.drawable.ic_bat_off);
                break;
            default:
                buttonBatBackground = getActivity().getDrawable(R.drawable.ic_bat_wait);
                break;
        }
        switch((int)can_parameter.channel_PwtState.getVariable()){
            case 0x60:
                buttonPwtBackground = getActivity().getDrawable(R.drawable.ic_pwt_on);
                break;
            case 0x10:
                buttonPwtBackground = getActivity().getDrawable(R.drawable.ic_bat_off);
                break;
            default:
                buttonPwtBackground = getActivity().getDrawable(R.drawable.ic_bat_wait);
                break;
        }
        switch((int)can_parameter.channel_FcsState.getVariable()){
            case 0x80:
                buttonFcsBackground = getActivity().getDrawable(R.drawable.ic_fc_on);
                break;
            case 0x10:
                buttonFcsBackground = getActivity().getDrawable(R.drawable.ic_fc_off);
                break;
            default:
                buttonFcsBackground = getActivity().getDrawable(R.drawable.ic_fc_wait);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       getActivity().setTitle(R.string.title_dashboard);
    }
}
