package com.greengt.sylvain.ggt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.TreeMap;

import static com.greengt.sylvain.ggt.R.color.colorBlueAccent;
import static com.greengt.sylvain.ggt.R.color.colorOrange;
import static com.greengt.sylvain.ggt.R.color.colorText;

/**
 * Created by Sylvain on 18.11.2016.
 */

public class CAN_Channel_Adapter extends ArrayAdapter<String> {

    private Context context;
    private TreeMap<String, CAN_Channel> treeMap;
    private String[] mapKeys;
    int selectedPosition = 0;
    TextView name;
    TextView data;
    TextView unit;

    public CAN_Channel_Adapter(Context context, TreeMap<String, CAN_Channel> treeMap) {
        super(context, R.layout.can_channel_lv_row);

        this.context = context;
        this.treeMap = treeMap;
        mapKeys = this.treeMap.keySet().toArray(new String[getCount()]);
    }

    public int getCount() {
        return treeMap.size();
    }

    public String getItem(int position) {
        return String.valueOf(treeMap.get(mapKeys[position]));
    }

    //public String getItemId(int position) {return mapKeys[position];}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater deviceInflater = LayoutInflater.from(getContext());
        final View listViewRow = deviceInflater.inflate(R.layout.can_channel_lv_row, parent, false);

        final CAN_Channel can_channel = (CAN_Channel) treeMap.get(mapKeys[position]);

        String nameString = can_channel.getName() + "";
        String unitString = "[" + can_channel.getUnit() + "]";
        String dataString = String.format("%.2f", can_channel.getVariable()) + " ";

        name = (TextView) listViewRow.findViewById(R.id.name);
        data = (TextView) listViewRow.findViewById(R.id.data);
        unit = (TextView) listViewRow.findViewById(R.id.unit);
        if(can_channel.isSelected()){
            updateTextColor(can_channel.getColor() == CAN_Channel.COLOR_ORANGE ?
                    listViewRow.getResources().getColor(colorOrange):listViewRow.getResources().getColor(colorBlueAccent));
        }else{
            updateTextColor(listViewRow.getResources().getColor(colorText));
        }

        name.setText(nameString);
        data.setText(dataString);
        unit.setText(unitString);
        this.notifyDataSetChanged();
        return listViewRow;
    }

    private void updateTextColor(int color){
        name.setTextColor(color);
        data.setTextColor(color);
        unit.setTextColor(color);
    }

}