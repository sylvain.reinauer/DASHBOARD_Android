package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     04.09.2018
 * Fichier:  class CAN_Channel
 * Sujet:    Description d'une variable/channel issue d'une trame CAN/CAN_frame
 *
 */
public class CAN_Channel {
    public static final Integer COLOR_ORANGE = 0;
    public static final Integer COLOR_BLUE = 1;

    private byte bitStartPosition;
    private byte bitSize;
    private String name;
    private String unit;
    private float variable;
    private float factor;
    private double maxValue;
    private double minValue;
    private int id;
    private boolean isSelected = false;
    private boolean isSigned = false;
    private Integer color = null;


    public CAN_Channel(String name, int id, byte bitStartPosition, byte bitSize, String unit, float factor, double maxValue, double minValue, boolean isSigned) {
        this.bitStartPosition = bitStartPosition;
        this.bitSize = bitSize;
        this.name = name;
        this.id = id;
        this.unit = unit;
        this.factor = factor;
        this.maxValue = maxValue;
        this.minValue = minValue;
        variable = 0;
        this.isSigned = isSigned;
    }

    public void setId (int id){
        this.id = id;
    }

    public void setVariable (float variable){
        this.variable = variable;
    }

    public float getVariable() {
        return variable;
    }

    public String getName() {
        return name;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public boolean isSigned() {
        return isSigned;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getUnit() {
        return unit;
    }

    public byte getBitSize() {
        return bitSize;
    }

    public byte getBitStartPosition() {
        return bitStartPosition;
    }

    public double getFactor() {
        return factor;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public double getMinValue() {
        return minValue;
    }

    public float getPercentValue(){
        return (float)(100/(this.maxValue-this.minValue)*this.getVariable());
    }
}
