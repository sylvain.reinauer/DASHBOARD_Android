package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     17.08.2018
 * Fichier:  ActivityStart
 * Sujet:    L'activité ActivityStart est l'activité de lancement de l'application elle effectue
 *           une petite animation et lance l'activité suivante lors de la fin de cell-ci.
 *
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.content.ContentValues.TAG;

public class ActivityMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    private double i = 0, sinX = 0;
    CAN_Parameter can_parameter = CAN_Parameter.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displaySelectedFragment(R.id.nav_dashboard);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref_0x170 = database.getReference("170");
        DatabaseReference ref_0x1020 = database.getReference("1020");
        DatabaseReference ref_0x1021 = database.getReference("1021");
        DatabaseReference ref_0x1022 = database.getReference("1022");
        DatabaseReference ref_0x1023 = database.getReference("1023");
        DatabaseReference ref_0x1024 = database.getReference("1024");
        DatabaseReference ref_0x1025 = database.getReference("1025");
        DatabaseReference ref_0x1028 = database.getReference("1028");

       ref_0x170.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x170 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x170.setDATA(can_firebase_ref_0x170.DATA);
                can_parameter.frame_0x170.setDLC(can_firebase_ref_0x170.DLC);
                can_parameter.frame_0x170.setFORMAT(can_firebase_ref_0x170.FORMAT);
                can_parameter.frame_0x170.setTIME(can_firebase_ref_0x170.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1020.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1020 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1020.setDATA(can_firebase_ref_0x1020.DATA);
                can_parameter.frame_0x1020.setDLC(can_firebase_ref_0x1020.DLC);
                can_parameter.frame_0x1020.setFORMAT(can_firebase_ref_0x1020.FORMAT);
                can_parameter.frame_0x1020.setTIME(can_firebase_ref_0x1020.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1021.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1021 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1021.setDATA(can_firebase_ref_0x1021.DATA);
                can_parameter.frame_0x1021.setDLC(can_firebase_ref_0x1021.DLC);
                can_parameter.frame_0x1021.setFORMAT(can_firebase_ref_0x1021.FORMAT);
                can_parameter.frame_0x1021.setTIME(can_firebase_ref_0x1021.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1022.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1022 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1022.setDATA(can_firebase_ref_0x1022.DATA);
                can_parameter.frame_0x1022.setDLC(can_firebase_ref_0x1022.DLC);
                can_parameter.frame_0x1022.setFORMAT(can_firebase_ref_0x1022.FORMAT);
                can_parameter.frame_0x1022.setTIME(can_firebase_ref_0x1022.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1023.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1023 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1023.setDATA(can_firebase_ref_0x1023.DATA);
                can_parameter.frame_0x1023.setDLC(can_firebase_ref_0x1023.DLC);
                can_parameter.frame_0x1023.setFORMAT(can_firebase_ref_0x1023.FORMAT);
                can_parameter.frame_0x1023.setTIME(can_firebase_ref_0x1023.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1024.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1024 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1024.setDATA(can_firebase_ref_0x1024.DATA);
                can_parameter.frame_0x1024.setDLC(can_firebase_ref_0x1024.DLC);
                can_parameter.frame_0x1024.setFORMAT(can_firebase_ref_0x1024.FORMAT);
                can_parameter.frame_0x1024.setTIME(can_firebase_ref_0x1024.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1025.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1025 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1025.setDATA(can_firebase_ref_0x1025.DATA);
                can_parameter.frame_0x1025.setDLC(can_firebase_ref_0x1025.DLC);
                can_parameter.frame_0x1025.setFORMAT(can_firebase_ref_0x1025.FORMAT);
                can_parameter.frame_0x1025.setTIME(can_firebase_ref_0x1025.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x1025 updated data: "+ can_parameter.frame_0x1025.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        ref_0x1028.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CAN_Firebase_ref can_firebase_ref_0x1028 = dataSnapshot.getValue(CAN_Firebase_ref.class);
                can_parameter.frame_0x1028.setDATA(can_firebase_ref_0x1028.DATA);
                can_parameter.frame_0x1028.setDLC(can_firebase_ref_0x1028.DLC);
                can_parameter.frame_0x1028.setFORMAT(can_firebase_ref_0x1028.FORMAT);
                can_parameter.frame_0x1028.setTIME(can_firebase_ref_0x1028.TIME);
                //Toast.makeText(getBaseContext(), "frame 0x102 updated data: "+ can_parameter.frame_0x1028.getDATA() , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        animStartGauge();
    }

    public void animStartGauge(){
        new Thread(new Runnable() {
            public void run(){
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    for (int c = 1; c <=100; c++) {
                        try {
                            i = c;
                            sinX = Math.sin(Math.toRadians(c*1.8));
                            Thread.sleep(5);
                            //Log.d(TAG, "i:" + i);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    for (int c = 99; c >= 0; c--) {
                        try {
                            i = c;
                            sinX = Math.sin(Math.toRadians(-1.8*c+360));
                            Thread.sleep(5);
                            //Log.d(TAG, "i:" + i);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
    synchronized public double getH2TankPr(){
        return 100.;
    }

    synchronized public double getSinusChannel(){
        return sinX;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displaySelectedFragment(item.getItemId());
        return true;
    }


    private void displaySelectedFragment(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_dashboard:
                fragment = new FragmentDashboard();
                break;
            case R.id.nav_analitic:
                fragment = new FragmentAnalitycs();
                break;
            case R.id.nav_can_frames:
                fragment = new FragmentCANFrame();
                break;
            case R.id.nav_tools:
                fragment = new FragmentAboutGGT();
                break;
            default:
                fragment = new FragmentDashboard();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TAG", "MAINACTIVITY ONPAUSE");
    }
}
