package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     04.09.2018
 * Fichier:  class CAN_parameter
 * Sujet:    Instanciable qu'une seule fois cette class est l'objet de configuration du CAN accesible par la méthode getInstance()
 *
 */

public class CAN_Parameter {

    public static final byte BMS_STATE_ON = 110;
    public static final byte FCS_STATE_ON = 80;
    public static final byte PWT_STATE_ON = 60;

    public static final byte POSITION_B0 = 0;
    public static final byte POSITION_B1 = 8;
    public static final byte POSITION_B2 = 16;
    public static final byte POSITION_B3 = 24;
    public static final byte POSITION_B4 = 32;
    public static final byte POSITION_B5 = 40;
    public static final byte POSITION_B6 = 48;
    public static final byte POSITION_B7 = 56;
    public static final byte SIZE_1BIT = 1;
    public static final byte SIZE_1BYTE = 8;
    public static final byte SIZE_2BYTE = 16;
    public static final byte SIZE_4BYTE = 32;

    public static final Double FACTOR_1_10 = 0.1;
    public static final Double FACTOR_1 = 1.;
    public static final String UNIT_PR_BAR = "Bar";
    public static final String UNIT_TP_CELSIUS = "°C";
    public static final String UNIT_PERCENT = "%";
    public static final String UNIT_SPEED = "km/h";
    public static final String UNIT_POWER = "kW";
    public static final String UNITLESS = "-";

    public CAN_Frame frame_0x170;
    public CAN_Frame frame_0x1020;
    public CAN_Frame frame_0x1021;
    public CAN_Frame frame_0x1022;
    public CAN_Frame frame_0x1023;
    public CAN_Frame frame_0x1024;
    public CAN_Frame frame_0x1025;
    public CAN_Frame frame_0x1028;
    public CAN_Frame frame_0x1030;

    public CAN_Channel channel_H2TankPressure;
    public CAN_Channel channel_WaterInletTemp;
    public CAN_Channel channel_WaterOutletTemp;
    public CAN_Channel channel_OilInletTemp;
    public CAN_Channel channel_OilOutletTemp;
    public CAN_Channel channel_BmsState;
    public CAN_Channel channel_PwtState;
    public CAN_Channel channel_FcsState;
    public CAN_Channel channel_BatSOC;
    public CAN_Channel channel_PwtPower;
    public CAN_Channel channel_BatPower;
    public CAN_Channel channel_FcsPower;
    public CAN_Channel channel_ThrottlePosition;
    public CAN_Channel channel_FrontBrakePressure;
    public CAN_Channel channel_CarSpeed;
    public CAN_Channel channel_Bms_WL2;
    public CAN_Channel channel_Bms_WL3;
    public CAN_Channel channel_Pwt_WL2;
    public CAN_Channel channel_Pwt_WL3;
    public CAN_Channel channel_Fcs_WL2;
    public CAN_Channel channel_Fcs_WL3;

    private static CAN_Parameter can_parameter = new CAN_Parameter();
    private CAN_Parameter() {
        initParameter();
    }

    public void initParameter(){
        frame_0x170 = new CAN_Frame(0x170, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1020 = new CAN_Frame(0x1020, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1021 = new CAN_Frame(0x1021, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1022 = new CAN_Frame(0x1022, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1023 = new CAN_Frame(0x1023, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1024 = new CAN_Frame(0x1024, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1025 = new CAN_Frame(0x1025, CAN_Frame.BIG_ENDIAN_MOTOROLA);
        frame_0x1028 = new CAN_Frame(0x1028, CAN_Frame.LITTLE_ENDIAN_INTEL);
        frame_0x1030 = new CAN_Frame(0x1030, CAN_Frame.BIG_ENDIAN_MOTOROLA);

        channel_H2TankPressure = new CAN_Channel("H2TankPressure", 0x1028, POSITION_B1, SIZE_2BYTE,  UNIT_PR_BAR, 0.1f, 710, 0, false);
        channel_WaterInletTemp = new CAN_Channel("WaterInletTemp", 0x170, POSITION_B0, SIZE_2BYTE, UNIT_TP_CELSIUS, 0.1f, 120, 0, true);
        channel_WaterOutletTemp = new CAN_Channel("WaterOutletTemp", 0x170, POSITION_B2, SIZE_2BYTE, UNIT_TP_CELSIUS, 0.1f, 120, 0, true);
        channel_OilInletTemp = new CAN_Channel("OilInletTemp", 0x170, POSITION_B4, SIZE_2BYTE, UNIT_TP_CELSIUS, 0.1f, 120, 0, true);
        channel_OilOutletTemp = new CAN_Channel("OilOutletTemp", 0x170, POSITION_B6, SIZE_2BYTE, UNIT_TP_CELSIUS, 1, 120, 0, true);
        channel_BmsState = new CAN_Channel("BmsState", 0x1020, POSITION_B0, SIZE_1BYTE, UNITLESS, 1, 120, 0, false);
        channel_PwtState = new CAN_Channel("PwtState", 0x1020, POSITION_B2, SIZE_1BYTE, UNITLESS, 1, 120, 0, false);
        channel_FcsState = new CAN_Channel("FcsState", 0x1020, POSITION_B4, SIZE_1BYTE, UNITLESS, 1, 120, 0, false);
        channel_BatSOC = new CAN_Channel("BatSOC", 0x1020, POSITION_B6, SIZE_2BYTE, UNIT_PERCENT, 0.1f, 100, 0, false);
        channel_PwtPower = new CAN_Channel("PwtPower", 0x1024, POSITION_B2, SIZE_2BYTE, UNIT_POWER, 1, 400, -400, true);
        channel_BatPower = new CAN_Channel("BatPower", 0x1024, POSITION_B4, SIZE_2BYTE, UNIT_POWER, 1, 300, -300, true);
        channel_FcsPower = new CAN_Channel("FcsPower", 0x1024, POSITION_B6, SIZE_1BYTE, UNIT_POWER, 1, 250, 0, false);
        channel_ThrottlePosition = new CAN_Channel("ThrottlePosition", 0x1024, POSITION_B7, SIZE_1BYTE, UNIT_PERCENT, 1, 100, 0, false);
        channel_FrontBrakePressure = new CAN_Channel("BrakePressure", 0x1025, POSITION_B2, SIZE_1BYTE, UNIT_PR_BAR, 1, 40, 0, false);
        channel_CarSpeed = new CAN_Channel("CarSpeed", 0x1025, POSITION_B7, SIZE_1BYTE, UNIT_SPEED, 1.2f, 320, 0, false);
        channel_Bms_WL2 = new CAN_Channel("BmsWL2", 0x1021, POSITION_B0, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);
        channel_Bms_WL3 = new CAN_Channel("BmsWL3", 0x1021, POSITION_B4, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);
        channel_Pwt_WL2 = new CAN_Channel("PwtWL2", 0x1022, POSITION_B0, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);
        channel_Pwt_WL3 = new CAN_Channel("PwtWL3", 0x1022, POSITION_B4, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);
        channel_Fcs_WL2 = new CAN_Channel("FcsWL2", 0x1023, POSITION_B0, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);
        channel_Fcs_WL3 = new CAN_Channel("FcsWL3", 0x1023, POSITION_B4, SIZE_4BYTE, UNITLESS, 1, 2147483646, 0, false);

        frame_0x170.addChannel(channel_WaterInletTemp);
        frame_0x170.addChannel(channel_WaterOutletTemp);
        frame_0x170.addChannel(channel_OilInletTemp);
        frame_0x170.addChannel(channel_OilOutletTemp);
        frame_0x1020.addChannel(channel_BmsState);
        frame_0x1020.addChannel(channel_PwtState);
        frame_0x1020.addChannel(channel_FcsState);
        frame_0x1020.addChannel(channel_BatSOC);
        frame_0x1021.addChannel(channel_Bms_WL2);
        frame_0x1021.addChannel(channel_Bms_WL3);
        frame_0x1022.addChannel(channel_Pwt_WL2);
        frame_0x1022.addChannel(channel_Pwt_WL3);
        frame_0x1023.addChannel(channel_Fcs_WL2);
        frame_0x1023.addChannel(channel_Fcs_WL3);
        frame_0x1024.addChannel(channel_PwtPower);
        frame_0x1024.addChannel(channel_BatPower);
        frame_0x1024.addChannel(channel_FcsPower);
        frame_0x1024.addChannel(channel_ThrottlePosition);
        frame_0x1025.addChannel(channel_FrontBrakePressure);
        frame_0x1025.addChannel(channel_CarSpeed);
        frame_0x1028.addChannel(channel_H2TankPressure);


    }
    public static CAN_Parameter getInstance()
    {
        return can_parameter;
    }
}
