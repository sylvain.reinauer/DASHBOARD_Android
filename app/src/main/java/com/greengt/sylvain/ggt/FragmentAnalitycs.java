package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     17.08.2018
 * Fichier:  Fragment FragmentAnalytics
 * Sujet:    Le fragment FragmentAnalytics gère l'UI décrit dans le fichier fragment_analytics.xml
 * Détails:  Le thread principale du fragment gère son cycle de vie, et l'affichage UI, deux thread sont lancé en parallèle.
 *           Thread updateGraphView:
 *           updateGraphView récupère les valeures, des channels sélectionné dans la listView, dans l'activtié principale ActivityMain
 *           Thread updateListView
 *           updateListView récupère toutes les valeures des channels dans l'activité principale ActivitymMain
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.TreeMap;

public class FragmentAnalitycs extends Fragment {
    public static final int NB_SERIE_MAX = 2;


    boolean runningThread = false;

    TreeMap<String, CAN_Channel> mTreeMap;
    ArrayAdapter adapter;
    ListView itemListView;

    LineGraphSeries<DataPoint> mSeries;
    GraphView graphView;
    Viewport viewport;

    public double lastX = 100;
    private double graph_channel_y_orange = 0;
    private double graph_channel_y_blue = 0;
    private int blue_position = 0;
    private int orange_position = 0;
    private int serieCount = 0;

    CAN_Channel can_channel_selected = null;
    LineGraphSeries<DataPoint>[] serie_tab = new LineGraphSeries[]{null, null};

    Thread updateGraphView;
    Thread updateListView;

    CAN_Parameter can_parameter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_analytic, container, false);

        graphView = (GraphView) rootView.findViewById(R.id.graphView);
        initGraphView();
        itemListView = (ListView) rootView.findViewById(R.id.itemListView);
        initListView();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.title_analytics);
    }

    @Override
    public void onStart() {
        super.onStart();
        runningThread = true;
        updateGraphView.start();
        updateListView.start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        updateGraphView.interrupt();
        updateListView.interrupt();
        runningThread = false;
        initChannels();
        serie_tab[CAN_Channel.COLOR_ORANGE] = null;
        serie_tab[CAN_Channel.COLOR_BLUE] = null;
        super.onPause();
    }

    public void initGraphView(){
        mSeries = new LineGraphSeries<>();
        graphView.setTitle("");
        graphView.setTitleColor(ContextCompat.getColor(getContext(), R.color.colorText));
        graphView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorBackroundPaleGrey));
        mSeries.setColor(ContextCompat.getColor(getContext(), R.color.colorOrange));

        viewport = graphView.getViewport();
        viewport.setXAxisBoundsManual(true);
        viewport.setMaxX(10);
        viewport.setScrollable(true);
        viewport.setScrollableY(true);
        viewport.scrollToEnd();

        graphView.getGridLabelRenderer().setHorizontalAxisTitle("2s/Div");
        graphView.getGridLabelRenderer().setVerticalAxisTitle(" ");
        graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    return "";
                } else {
                    return "  " + super.formatLabel(value, isValueX);
                }
            }
        });
        graphView.addSeries(mSeries);
        mSeries.appendData(new DataPoint(lastX++/10, 100),true, 100);

        updateGraphView = new Thread() {
            @Override
            public void run() {
                while (getActivity() != null && runningThread){
                    if(serieCount >= 1){
                        graph_channel_y_orange = mTreeMap.get(mTreeMap.keySet().toArray()[orange_position]).getVariable();
                        graph_channel_y_blue = mTreeMap.get(mTreeMap.keySet().toArray()[blue_position]).getVariable();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addEntry();
                            }
                        });
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }

    public void initListView(){
        can_parameter = CAN_Parameter.getInstance();
        mTreeMap = new TreeMap<String, CAN_Channel>();
        mTreeMap.put(can_parameter.channel_H2TankPressure.getName(), can_parameter.channel_H2TankPressure);
        mTreeMap.put(can_parameter.channel_WaterInletTemp.getName(), can_parameter.channel_WaterInletTemp);
        mTreeMap.put(can_parameter.channel_WaterOutletTemp.getName(), can_parameter.channel_WaterOutletTemp);
        mTreeMap.put(can_parameter.channel_OilInletTemp.getName(), can_parameter.channel_OilInletTemp);
        mTreeMap.put(can_parameter.channel_OilOutletTemp.getName(), can_parameter.channel_OilOutletTemp);
        mTreeMap.put(can_parameter.channel_BmsState.getName(), can_parameter.channel_BmsState);
        mTreeMap.put(can_parameter.channel_PwtState.getName(), can_parameter.channel_PwtState);
        mTreeMap.put(can_parameter.channel_FcsState.getName(), can_parameter.channel_FcsState);
        mTreeMap.put(can_parameter.channel_BatSOC.getName(), can_parameter.channel_BatSOC);
        mTreeMap.put(can_parameter.channel_Bms_WL2.getName(), can_parameter.channel_Bms_WL2);
        mTreeMap.put(can_parameter.channel_Bms_WL3.getName(), can_parameter.channel_Bms_WL3);
        mTreeMap.put(can_parameter.channel_Pwt_WL2.getName(), can_parameter.channel_Pwt_WL2);
        mTreeMap.put(can_parameter.channel_Pwt_WL3.getName(), can_parameter.channel_Pwt_WL3);
        mTreeMap.put(can_parameter.channel_Fcs_WL2.getName(), can_parameter.channel_Fcs_WL2);
        mTreeMap.put(can_parameter.channel_Fcs_WL3.getName(), can_parameter.channel_Fcs_WL3);
        mTreeMap.put(can_parameter.channel_PwtPower.getName(), can_parameter.channel_PwtPower);
        mTreeMap.put(can_parameter.channel_BatPower.getName(), can_parameter.channel_BatPower);
        mTreeMap.put(can_parameter.channel_FcsPower.getName(), can_parameter.channel_FcsPower);
        mTreeMap.put(can_parameter.channel_ThrottlePosition.getName(), can_parameter.channel_ThrottlePosition);
        mTreeMap.put(can_parameter.channel_FrontBrakePressure.getName(), can_parameter.channel_FrontBrakePressure);
        mTreeMap.put(can_parameter.channel_CarSpeed.getName(), can_parameter.channel_CarSpeed);

        adapter = new CAN_Channel_Adapter(getActivity(), (TreeMap<String, CAN_Channel>) mTreeMap);
        itemListView.setAdapter(adapter);
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                can_channel_selected = mTreeMap.get(mTreeMap.keySet().toArray()[position]);

                if(can_channel_selected.isSelected()){
                    removeSerie(can_channel_selected.getColor());
                }
                else{
                   if(serie_tab[CAN_Channel.COLOR_ORANGE] == null){
                         addSerie(CAN_Channel.COLOR_ORANGE, getResources().getColor(R.color.colorOrange));
                        orange_position = position;
                    }
                    else if(serie_tab[CAN_Channel.COLOR_BLUE] == null){
                        addSerie(CAN_Channel.COLOR_BLUE, getResources().getColor(R.color.colorBlueAccent));
                        blue_position = position;
                    }
                }
            }
        });

        updateListView = new Thread() {
            @Override
            public void run() {

                while (getActivity() != null && runningThread){
                    updateChannels();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                    try {
                         Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

    }

    synchronized private void addSerie(int indice, int color){
        serieCount++;
        serie_tab[indice] = new LineGraphSeries<>();
        serie_tab[indice].setColor(color);
        graphView.addSeries(serie_tab[indice]);
        can_channel_selected.setSelected(true);
        can_channel_selected.setColor(indice);
    }

    synchronized private void removeSerie(int indice){
        serieCount--;
        graphView.removeSeries(serie_tab[indice]);
        serie_tab[indice] = null;
        can_channel_selected.setSelected(false);
        can_channel_selected.setColor(null);
    }


   synchronized private void addEntry(){
        lastX++;
        if(serie_tab[0] != null){
            serie_tab[0].appendData(new DataPoint(lastX/10, graph_channel_y_orange),false, 100);
        }
        if(serie_tab[1] != null){
            serie_tab[1].appendData(new DataPoint(lastX/10, graph_channel_y_blue),false, 100);
        }
        viewport.scrollToEnd();
    }

    synchronized private void updateChannels(){
        /*double y = 0, sinX = 0;
        y =((ActivityMain)getActivity()).getH2TankPr();
        sinX = ((ActivityMain)getActivity()).getSinusChannel();
        can_parameter.channel_H2TankPressure.setVariable(y);
        can_parameter.channel_WaterInletTemp.setVariable(-100*sinX);
        can_parameter.channel_WaterOutletTemp.setVariable(100*sinX);
        can_parameter.channel_OilInletTemp.setVariable(y);
        can_parameter.channel_OilOutletTemp.setVariable(y*0.7);
        can_parameter.channel_BmsState.setVariable(y);
        can_parameter.channel_PwtState.setVariable(100*sinX);
        can_parameter.channel_FcsState.setVariable(100*sinX);
        can_parameter.channel_BatSOC.setVariable(100*sinX);
        can_parameter.channel_Bms_WL2.setVariable(100*sinX);
        can_parameter.channel_Bms_WL3.setVariable(70*sinX);
        can_parameter.channel_Pwt_WL2.setVariable(100*sinX);
        can_parameter.channel_Pwt_WL3.setVariable(100*sinX);
        can_parameter.channel_Fcs_WL2.setVariable(100*sinX);
        can_parameter.channel_Fcs_WL3.setVariable(100*sinX);
        can_parameter.channel_PwtPower.setVariable(100*sinX);
        can_parameter.channel_BatPower.setVariable(-100*sinX);
        can_parameter.channel_FcsPower.setVariable(100*sinX);
        can_parameter.channel_ThrottlePosition.setVariable(100*sinX);
        can_parameter.channel_FrontBrakePressure.setVariable(100*sinX);
        //can_parameter.channel_CarSpeed.setVariable(100*sinX);
        can_parameter.channel_CarSpeed.setVariable(can_parameter.channel_CarSpeed.getVariable());*/
       // can_parameter.channel_CarSpeed.setVariable(can_parameter.frame_0x1025.getCan_channel_List().get("CarSpeed").getVariable());
    }

    private void initChannels(){
        for(TreeMap.Entry<String, CAN_Channel> entry : mTreeMap.entrySet()) {
            CAN_Channel value = entry.getValue();
            value.setSelected(false);
            value.setColor(null);
        }

    }
}
