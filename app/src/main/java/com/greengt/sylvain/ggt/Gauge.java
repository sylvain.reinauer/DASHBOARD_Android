package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     17.08.2018
 * Fichier:  class Gauge
 * Sujet:    La class Gauge décrit un objet qui compose en élement de type gauge sur l'UI
 *
 */

import android.view.View;

public class Gauge {
    private View background;
    private View indicator;
    private float angle;
    private float angleMax;
    private float angleMin;

    public Gauge(View background, View indicator, float angleMin, float angleMax) {
        this.background = background;
        this.indicator = indicator;
        this.angleMax = angleMax;
        this.angleMin = angleMin;
    }
/*
La méthode setAngle:
Met à jour l'attribut angle ainsi que l'angle de la vue indicator (aiguille de la gauge)
Doit être appelée sur le thread UI (getActivity().runOnUiThread(new Runnable())
afin de pouvoir visualiser le changment sur une interface
 */
    public void setAngle(float _angle){
        if(_angle <= this.angleMax && _angle >= this.angleMin){
            indicator.setRotation(_angle);
        }
        else if(_angle <= this.angleMin) {
            indicator.setRotation(this.angleMin);
        }
        else{indicator.setRotation(this.angleMax);}
    }
    public void setAngleFromPercent(float percent){
        float a = (angleMax+Math.abs(angleMin))/100;
        float b = angleMin;
        this.angle = a*percent+b;
        setAngle(angle);
    }
}
