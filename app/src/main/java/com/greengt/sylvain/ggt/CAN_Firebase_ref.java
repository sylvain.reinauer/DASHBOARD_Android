package com.greengt.sylvain.ggt;

public class CAN_Firebase_ref {
    String DATA;
    int DLC;
    String FORMAT;
    double TIME;

    public CAN_Firebase_ref() {
    }

    public CAN_Firebase_ref(String DATA, int DLC, String FORMAT, double TIME) {
        this.DATA = DATA;
        this.DLC = DLC;
        this.FORMAT = FORMAT;
        this.TIME = TIME;
    }

}
