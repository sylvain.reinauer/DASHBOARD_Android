package com.greengt.sylvain.ggt;
/*
* Auteur:   Sylvain Reinauer
* Date:     04.09.2018
* Fichier:  class CAN_Frame
* Sujet:    La class CAN_Frame décrit la composition d'une trame CAN
*
 */

import java.util.TreeMap;

public class CAN_Frame {
    public static final String LITTLE_ENDIAN_INTEL = "Little Endian"; //LH
    public static final String BIG_ENDIAN_MOTOROLA= "Big Endian"; //HL
    public static final String FORMAT_EXTENDED = "2.0B"; //Identifiant sur 29 bits
    public static final String FORMAT_NORMAL= "2.0A"; //Identifiant sur 11 bits
    private static final String NO_DATA = "00";

    private int id;
    private String byteOrder;
    private int DLC; // Data Lenght Code (1-8)
    private boolean isExtended;
    private String FORMAT;
    private String DATA;
    private int[] data;
    private double timeOut, previousTime, TIME;
    private TreeMap<String,CAN_Channel> can_channel_List;
    CAN_Parameter can_parameter = CAN_Parameter.getInstance();

    public CAN_Frame() {

    }

    public CAN_Frame(int id, String byteOrder) {
        this.id = id;
        this.byteOrder = byteOrder;
        initCANFrame();
    }

    public void initCANFrame(){
        can_channel_List = new TreeMap<>();
        DATA = NO_DATA;
        FORMAT = FORMAT_NORMAL;
        data = new int[8];
        stringDataToByteData();
        timeOut = 0;
        TIME = 0;
        previousTime = 0;
        DLC = 8;
    }

    public void addChannel (CAN_Channel channel){
        can_channel_List.put(channel.getName(),channel);
        channel.setId(this.id);
    }

    synchronized public void stringDataToByteData(){
            for (int i = 0; i < DLC * 2; i += 2) {
                data[i / 2] = (int) ((Character.digit(DATA.charAt(i), 16) << 4)
                        + Character.digit(DATA.charAt(i+1), 16));
            }
    }

    synchronized public void setDATA(String DATA) {
        this.DATA = DATA;
        stringDataToByteData();
        udpateChannel();
    }

    synchronized private void udpateChannel(){

        for (TreeMap.Entry<String,CAN_Channel> entry : can_channel_List.entrySet()) {
            CAN_Channel can_channel = entry.getValue();
            int positionStart = can_channel.getBitStartPosition()/8;
            int positionEnd = (can_channel.getBitStartPosition()/8)+can_channel.getBitSize()/8;
            int tempVariable = 0;
            int cpt = 0;
            if(this.byteOrder == BIG_ENDIAN_MOTOROLA){ // HL
                cpt = can_channel.getBitSize()-8;
                for(int i = positionStart; i < positionEnd; i++){
                    tempVariable = tempVariable + (data[i] << cpt);
                    cpt-=8;
                }
            }
            else if(this.byteOrder == LITTLE_ENDIAN_INTEL){ // LH
                for(int i = positionStart; i < positionEnd; i++){
                    tempVariable = tempVariable + (data[i] << (cpt*8));
                    cpt++;
                }
            }
            if(can_channel.isSigned() && (tempVariable >> can_channel.getBitSize()-1 == 1)){
                tempVariable = (~tempVariable) + 1;
            }
            can_channel.setVariable(tempVariable*(float)can_channel.getFactor());
            entry.setValue(can_channel);
        }
    }
    public String getDATA() {
        return DATA;
    }

    public String getStringDataHex() {
        return String.format("DATA: %02X %02X %02X %02X %02X %02X %02X %02X", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    }

    public int getId() {
        return id;
    }
    public String getStringId(){
        return ("0x1028");
    }

    public void setFORMAT(String FORMAT) {
        this.FORMAT = FORMAT;
    }

    public void setDLC(int DLC) {
        this.DLC = DLC;
    }

    public void setTIME(double TIME) {
        previousTime = this.TIME;
        this.TIME = TIME;
        timeOut = (TIME-previousTime)/1000;
    }

    public String getFORMAT() {
        return FORMAT;
    }

    public double getTimeOut() {
        return timeOut;
    }

    public int getDLC() {
        return DLC;
    }

    public TreeMap<String, CAN_Channel> getCan_channel_List() {
        return can_channel_List;
    }
}
