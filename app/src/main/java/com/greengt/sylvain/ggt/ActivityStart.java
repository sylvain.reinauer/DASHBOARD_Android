package com.greengt.sylvain.ggt;
/*
 * Auteur:   Sylvain Reinauer
 * Date:     17.08.2018
 * Fichier:  ActivityStart
 * Sujet:    L'activité ActivityStart est l'activité de lancement de l'application elle effectue
 *           une petite animation et lance l'activité suivante lors de la fin de cell-ci.
 *
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

public class ActivityStart extends AppCompatActivity {

    ImageView GGT_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorFragmentBackground));
        setContentView(R.layout.activity_start);

        GGT_image = (ImageView) findViewById(R.id.iGGT);

        ObjectAnimator anim_logoAppears = ObjectAnimator.ofFloat(GGT_image, "alpha", 0, 1f).setDuration(1000);
        ObjectAnimator anim_logoDisapears = ObjectAnimator.ofFloat(GGT_image, "alpha", 1, 0f).setDuration(1000);

        final AnimatorSet as = new AnimatorSet();
        as.playSequentially(anim_logoAppears, anim_logoDisapears);
        as.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //as.start();
                startMainActivity();
            }
        });
        as.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    void startMainActivity(){
        Intent intent = new Intent(this, ActivityMain.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }
}
