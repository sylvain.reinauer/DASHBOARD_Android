package com.greengt.sylvain.ggt;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.TreeMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCANFrame extends Fragment {


    public FragmentCANFrame() {
        // empty public constructor
    }

    boolean runningThread = false;

    TreeMap<Integer, CAN_Frame> mTreeMap;
    ArrayAdapter adapter;
    ListView itemListView;

    Thread updateListView;

    CAN_Parameter can_parameter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_can_frames, container, false);

        itemListView = (ListView) rootView.findViewById(R.id.itemListViewCanFrame);
        initListView();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.title_can_frame);
    }

    @Override
    public void onStart() {
        super.onStart();
        runningThread = true;
        updateListView.start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        updateListView.interrupt();
        runningThread = false;
        super.onPause();
    }


    public void initListView(){
        can_parameter = CAN_Parameter.getInstance();
        mTreeMap = new TreeMap<Integer, CAN_Frame>();
        mTreeMap.put(can_parameter.frame_0x170.getId(), can_parameter.frame_0x170);
        mTreeMap.put(can_parameter.frame_0x1020.getId(), can_parameter.frame_0x1020);
        mTreeMap.put(can_parameter.frame_0x1021.getId(), can_parameter.frame_0x1021);
        mTreeMap.put(can_parameter.frame_0x1022.getId(), can_parameter.frame_0x1022);
        mTreeMap.put(can_parameter.frame_0x1023.getId(), can_parameter.frame_0x1023);
        mTreeMap.put(can_parameter.frame_0x1024.getId(), can_parameter.frame_0x1024);
        mTreeMap.put(can_parameter.frame_0x1025.getId(), can_parameter.frame_0x1025);
        mTreeMap.put(can_parameter.frame_0x1028.getId(), can_parameter.frame_0x1028);


        adapter = new CAN_Frame_Adapter(getActivity(), (TreeMap<Integer, CAN_Frame>) mTreeMap);
        itemListView.setAdapter(adapter);

        updateListView = new Thread() {
            @Override
            public void run() {

                while (getActivity() != null && runningThread){
                    updateFrames();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

    }


    synchronized private void updateFrames(){

    }

}
